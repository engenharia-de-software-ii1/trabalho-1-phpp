<?php
    function soma ($a, $b){
        return $a + $b;
    }

    function mult($a, $b){
        return $a * $b;
    }

    function veri_soma ($a, $b, $c){
        if (($a + $b) + $c == $a + ($b + $c))
            if ($a + $b == $b + $a)
                if ($a + 0 == $a && $b + 0 == $b)
                    if ($a + (-$a) == 0 && $b + (-$b) == 0)
                        return "Todos os axiomas foram verdade.";
        else
            return "Algum dos axiomas foi falso";
    }

    function veri_produto ($a, $b, $c){
        if ( ($a * $b) * $c == $a * ($b * $c) )
            if ($a * $b == $b * $a)
                if ($a * 1 == $a && $b * 1 == $b)
                    if ($a * pow($a, -1) == 1 && $b * pow($b, -1) == 1)
                        return "Todos os axiomas foram verdade.";
                    else
                        return "O quarto axioma foi falso. Pois um dos valores é 0.";
        else 
            return "Algum dos axiomas foi falso";;
    }

    $data = json_decode(file_get_contents("php://input"), true);

    $a = $data[0];
    $b = $data[1];

    if($a == 0 || $b == 0){
        $error = "Erro, um dos valores é 0. Digite outro número.";
        $error = json_encode($error);
        echo ($error);
    }
    else {
        $r1 = soma($a, $b);
        $r2 = mult($a, $b);
        $r3 = veri_soma($a, $b, 1);
        $r4 = veri_produto($a, $b, 1);

        $r5 = [$r1, $r2, $r3, $r4];

        $res = json_encode($r5);
        echo $res;
    }
?>